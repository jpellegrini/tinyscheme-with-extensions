PREFIX ?= /usr/local

TS_VERSION ?= 1.42
TS_HASH ?= 17b0b1bffd22f3d49d5833e22a120b339039d2cfda0b46d6fc51dd2f01b407ad
TS_TAR = tinyscheme-${TS_VERSION}.tar.gz

RE_VERSION ?= 1.3
RE_HASH ?= 5582ce3f27a3e054ffb9b1da7d691901ca1c7ee183f05acffb51d8714eb8ff1b
RE_TAR = re-${RE_VERSION}.tar.gz

TSX_VERSION ?= v1.1.1
TSX_HASH ?= 46c47239a3bed27a112484a727d743adee35b1d13352f7c033021743d4c9d112
TSX_TAR = tinyscheme-tsx-${TSX_VERSION}.tar.gz

## TSION needs to be patched. it has hardcoded paths for includes and
## libs.
##
## TSION has no version number, and doesn't seem to be updated, but works!
#TSION_HASH= 0d0fb6686593e7b9f9b953047fcceaab8e7da4138bb5d4dc58b43d1bb6dce744
#TSION_TAR = csoft.tgz

all:
	make get
	make check
	make extract
	make build

get:
	wget https://sourceforge.net/projects/tinyscheme/files/tinyscheme/tinyscheme-${TS_VERSION}/${TS_TAR}/download -O ${TS_TAR}
	wget https://sourceforge.net/projects/tinyscheme/files/tinyscheme-regex/${RE_VERSION}/${RE_TAR}/download -O ${RE_TAR}
	wget https://gitlab.com/jpellegrini/tinyscheme-tsx/-/archive/${TSX_VERSION}/${TSX_TAR}
check:
	$(eval TS_COMPUTED_HASH != sha256sum ${TS_TAR} | cut -f 1 -d ' ')
	$(eval RE_COMPUTED_HASH != sha256sum ${RE_TAR} | cut -f 1 -d ' ')
#	$(eval TSION_COMPUTED_HASH != sha256sum ${TSION_TAR} | cut -f 1 -d ' ')
	@echo "TS HASH = ${TS_COMPUTED_HASH}"
	@echo "RE HASH = ${RE_COMPUTED_HASH}"
#	@echo "TSION HASH = ${TSION_COMPUTED_HASH}"
	@if [ "${TS_COMPUTED_HASH}" != "${TS_HASH}" ]; then \
	        echo "Tinyscheme hash not correct!"; \
	        exit -1; \
	fi
	@if [ "${RE_COMPUTED_HASH}" != "${RE_HASH}" ]; then \
	        echo "RE hash not correct!"; \
	        exit -1; \
	fi
#	@if [ "${TSION_COMPUTED_HASH}" != "${TSION_HASH}" ]; then \
#                echo "TSION hash not correct!"; \
#                exit -1; \
#        fi

extract:
	tar xzf tinyscheme-${TS_VERSION}.tar.gz
	(cd tinyscheme-${TS_VERSION} && tar xzf ../${TSX_TAR})
	(cd tinyscheme-${TS_VERSION} && tar xzf ../${RE_TAR})
#	if [ ! -d tinyscheme-${TS_VERSION}/csoft ]; then mkdir tinyscheme-${TS_VERSION}/csoft; fi
#	(cd tinyscheme-${TS_VERSION}/csoft/ && tar xzf ../../${TSION_TAR} && chmod -R u+w *)


build:
	make -C tinyscheme-${TS_VERSION}
	make -C tinyscheme-${TS_VERSION}/tinyscheme-tsx-${TSX_VERSION}
	make -C tinyscheme-${TS_VERSION}/re -f re.makefile SCHEME_H_DIR=..
#	make -C tinyscheme-${TS_VERSION}/csoft/tsion -f Makefile.linux

install:
	install -d ${PREFIX}/share/tinyscheme/
	install tinyscheme-${TS_VERSION}/scheme                                ${PREFIX}/bin/tinyscheme
	install tinyscheme-${TS_VERSION}/tinyscheme-tsx-${TSX_VERSION}/tsx.so  ${PREFIX}/share/tinyscheme/tsx.so
	install tinyscheme-${TS_VERSION}/re/re.so                              ${PREFIX}/share/tinyscheme/re.so

uninstall:
	rm -f ${PREFIX}/bin/tinyscheme  ${PREFIX}/share/tinyscheme/tsx.so  ${PREFIX}/share/tinyscheme/re.so

clean:
	 rm -rf tinyscheme-${TS_VERSION}
