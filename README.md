This is a Makefile which will download Tinyscheme, RE and TSX,
build them and install.

License for the Makefile is AGPLv3 or later.

License for Tinyscheme and TSX is the 3-clause BSD.
License for RE is a license similar to the zlib license, but
a bit different.
